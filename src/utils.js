/**
 * Copyright (c) 2018, Brandon D. Sara (https://bsara.gitlab.io/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/web-font-unit-converter/blob/master/LICENSE)
 */
const PT_PER_PX = 0.75;
const PX_PER_PT = (4 / 3);


export function convert({ convertFromValue, convertFromUnit, emBaseValue, emBaseUnit }) {
  let emValue;
  let ptValue;
  let pxValue;

  if (convertFromUnit === 'em') {
    emValue = convertFromValue;
    ptValue = fromEm(convertFromValue, ((emBaseUnit === 'pt') ? emBaseValue : pxToPt(emBaseValue)));
    pxValue = fromEm(convertFromValue, ((emBaseUnit === 'px') ? emBaseValue : ptToPx(emBaseValue)));
  } else {
    ptValue = ((convertFromUnit === 'pt') ? convertFromValue : pxToPt(convertFromValue));
    pxValue = ((convertFromUnit === 'px') ? convertFromValue : ptToPx(convertFromValue));

    if (!emValue && emBaseValue && emBaseValue.trim()) {
      let emBaseValueTemp = emBaseValue;

      if (convertFromUnit === 'pt' && emBaseUnit === 'px') {
        emBaseValueTemp = pxToPt(emBaseValue);
      } else if (convertFromUnit === 'px' && emBaseUnit === 'pt') {
        emBaseValueTemp = ptToPx(emBaseValue);
      }

      emValue = toEm(convertFromValue, emBaseValueTemp);
    }
  }

  return {
    emValue,
    ptValue,
    pxValue
  };
}


export function ptToPx(ptValue) {
  return (ptValue * PX_PER_PT);
}


export function pxToPt(pxValue) {
  return (pxValue * PT_PER_PX);
}


export function toEm(value, emBase) {
  return (value / emBase);
}


export function fromEm(emValue, emBase) {
  return (emValue * emBase);
}
