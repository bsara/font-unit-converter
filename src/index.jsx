/**
 * Copyright (c) 2018, Brandon D. Sara (https://bsara.gitlab.io/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/web-font-unit-converter/blob/master/LICENSE)
 */
import React from 'react';
import ReactDOM from 'react-dom';

import configureStore from './store';
import * as serviceWorker from './service-worker';

import App from './components/App';
import { Provider } from 'react-redux';

import './global-styles/global.css';



ReactDOM.render(
  <Provider store={configureStore()}>
    <App />
  </Provider>,

  document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
