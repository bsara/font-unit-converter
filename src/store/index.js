/**
 * Copyright (c) 2018, Brandon D. Sara (https://bsara.gitlab.io/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/web-font-unit-converter/blob/master/LICENSE)
 */
import { createStore } from 'redux';
import reducer from './reducer';



export default function configureStore() {
  return createStore(reducer);
}
