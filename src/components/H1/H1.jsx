/**
 * Copyright (c) 2018, Brandon D. Sara (https://bsara.gitlab.io/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/web-font-unit-converter/blob/master/LICENSE)
 */
import React from 'react';

import classnames from 'classnames';

import './H1.css';



const H1 = React.memo(function({ className, children, ...props }) {
  return (
    <h1 {...props} className={classnames('h1', className)}>
      {children}
    </h1>
  );
});


export default H1;
