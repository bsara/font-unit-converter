/**
 * Copyright (c) 2018, Brandon D. Sara (https://bsara.gitlab.io/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/web-font-unit-converter/blob/master/LICENSE)
 */
import React from 'react';



export default class Static extends React.Component {
  shouldComponentUpdate() { // eslint-disable-line class-methods-use-this
    return false;
  }


  render() {
    return this.props.children;
  }
}
