/**
 * Copyright (c) 2018, Brandon D. Sara (https://bsara.gitlab.io/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/web-font-unit-converter/blob/master/LICENSE)
 */
import React from 'react';

import classnames from 'classnames';

import './UnitSelect.css';



export default function UnitSelect({ excludeEm, className, onChange, ...props }) {
  return (
    <select {...props} className={classnames('unit-select', className)} onChange={(e) => onChange(e.target.value)}>
      {!excludeEm && (
        <option value="em">em/rem</option>
      )}
      <option value="pt">pt</option>
      <option value="px">px</option>
    </select>
  );
}
