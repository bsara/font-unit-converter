/**
 * Copyright (c) 2018, Brandon D. Sara (https://bsara.gitlab.io/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/web-font-unit-converter/blob/master/LICENSE)
 */
import React from 'react';

import classnames from 'classnames';

import './Input.css';



export default function Input({ className, onChange, ...props }) {
  return (
    <input
      {...props}
      className={classnames('input', className)}
      type="number"
      onChange={(e) => onChange(e.target.value)} />
  );
}
