/**
 * Copyright (c) 2018, Brandon D. Sara (https://bsara.gitlab.io/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/web-font-unit-converter/blob/master/LICENSE)
 */
import React from 'react';

import classnames from 'classnames';
import getBoundFunc from 'get-bound-func';

import GridLayout from '@bsara/react-layouts/GridLayout';
import Input from './Input';
import LinearLayout from '@bsara/react-layouts/LinearLayout';
import ResultsCard from '../ResultsCard';
import Static from '../Static';
import UnitSelect from './UnitSelect';

import './Form.css';



export default class Form extends React.PureComponent {

  state = {
    value:     undefined,
    valueUnit: 'px',

    emBase:     16,
    emBaseUnit: 'px'
  };


  render() {
    return (
      <LinearLayout className={classnames('form-layout', this.props.className)} direction="vertical">
        <Static>
          <GridLayout className="fields">
            <Input label="Value to Convert" onChange={getBoundFunc(this, _onChangeValue)} />
            <UnitSelect defaultValue="px" onChange={getBoundFunc(this, _onChangeValueUnit)} />

            <Input label="EM Base" defaultValue="16" onChange={getBoundFunc(this, _onChangeEmBase)} />
            <UnitSelect excludeEm defaultValue="px" onChange={getBoundFunc(this, _onChangeEmBaseUnit)} />
          </GridLayout>
        </Static>

        <ResultsCard {...this.state} />
      </LinearLayout>
    );
  }
}


// region Event Handlers

/**
 * @this {Form}
 * @private
 */
function _onChangeValue(newValue) {
  this.setState({ value: newValue });
}


/**
 * @this {Form}
 * @private
 */
function _onChangeValueUnit(newValue) {
  this.setState({ valueUnit: newValue });
}


/**
 * @this {Form}
 * @private
 */
function _onChangeEmBase(newValue) {
  this.setState({ emBase: newValue });
}


/**
 * @this {Form}
 * @private
 */
function _onChangeEmBaseUnit(newValue) {
  this.setState({ emBaseUnit: newValue });
}

// endregion
