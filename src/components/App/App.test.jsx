/**
 * Copyright (c) 2018, Brandon D. Sara (https://bsara.gitlab.io/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/web-font-unit-converter/blob/master/LICENSE)
 */
import React from 'react';
import ReactDOM from 'react-dom';

import App from '.';



it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
