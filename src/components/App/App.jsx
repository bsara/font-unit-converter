/**
 * Copyright (c) 2018, Brandon D. Sara (https://bsara.gitlab.io/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/web-font-unit-converter/blob/master/LICENSE)
 */
import React from 'react';

import Form from '../Form';
import H1 from '../H1';
import LinearLayout from '@bsara/react-layouts/LinearLayout';

import './App.css';



export default class App extends React.Component {
  render() {
    return (
      <LinearLayout id="app" direction="vertical">
        <header id="appHeader">
          <H1>Web Font Unit Converter</H1>
        </header>

        <main id="appContent">
          <Form />
        </main>

        <footer id="appFooter">TODO</footer>
      </LinearLayout>
    );
  }
}
